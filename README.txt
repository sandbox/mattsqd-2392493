Introduction
------------

Fixes compatibility issues between Media WYSIWYG and CKEditor. If you have both these modules enabled and your WYSIWYG fields are failing to load after you place media tags in your summaries, this module may help you.

Requirements
------------

https://www.drupal.org/project/media
https://www.drupal.org/project/ckeditor

Installation
------------

Just enable the module!
